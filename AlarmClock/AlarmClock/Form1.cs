using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.IO;

namespace AlarmClock
{
    public partial class Form1 : Form
    {
        String m_strwakeuptime;
        bool m_balarmactive = false;

        public Form1()
        {
            InitializeComponent();
            
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("HH:mm:ss");
            lblDate.Text = DateTime.Now.ToString("dddd dd MMMM yyyy");
            //lblDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            //lblDate.Text = GetTimeRemaining(DateTime.Parse("07/10/2008 08:00:00"), "08:00:00");

            if (chkAlarmActive.Checked == true)
            {            
                lblSleepRemaining.Text = "Sleep Remaining: " + GetTimeRemaining(DateTime.Now, m_strwakeuptime);
                

                DateTime dtWake = new DateTime();
                dtWake = DateTime.Parse(m_strwakeuptime);

                DateTime dtWakeForward = new DateTime();            
                TimeSpan tsHour = new TimeSpan(0, 0, 5);

                dtWakeForward = dtWake + tsHour;

                DateTime dtCurrent = new DateTime(dtWake.Year, dtWake.Month, dtWake.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                if (dtCurrent > dtWake && dtCurrent < dtWakeForward)
                {
                    if (m_balarmactive == false)
                    {
                        m_balarmactive = true;

                        if (txtAction.Text != "")
                        {
                            try
                            {
                                Process myProcess = new Process();

                                myProcess.StartInfo.FileName = (char)34 + "C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe" + (char)34;
                                myProcess.StartInfo.Arguments = (char)34 + txtAction.Text + (char)34;

                                myProcess.Start();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Unable to start process: " + ex.Message);
                            }
                        }
                    }
                }

                if (dtCurrent > dtWakeForward)
                {
                    m_balarmactive = false;
                }
            }
        }

        private void opt800_CheckedChanged(object sender, EventArgs e)
        {
            m_strwakeuptime = "08:00:00";
            lblAlarm.Text = m_strwakeuptime;
            //Process.Start("C:\\Temp\\Email.txt");
        }

        private void opt730_CheckedChanged(object sender, EventArgs e)
        {
            m_strwakeuptime = "07:30:00";
            lblAlarm.Text = m_strwakeuptime;
        }

        private void opt700_CheckedChanged(object sender, EventArgs e)
        {
            m_strwakeuptime = "07:00:00";
            lblAlarm.Text = m_strwakeuptime;
        }

        private void opt645_CheckedChanged(object sender, EventArgs e)
        {
            m_strwakeuptime = "06:45:00";
            lblAlarm.Text = m_strwakeuptime;
        }

        private void opt630_CheckedChanged(object sender, EventArgs e)
        {
            m_strwakeuptime = "06:30:00";
            lblAlarm.Text = m_strwakeuptime;
        }

        private void opt600_CheckedChanged(object sender, EventArgs e)
        {
            m_strwakeuptime = "06:00:00";
            lblAlarm.Text = m_strwakeuptime;
        }

        private void opt545_CheckedChanged(object sender, EventArgs e)
        {
            m_strwakeuptime = "05:45:00";
            lblAlarm.Text = m_strwakeuptime;
        }

        private void opt530_CheckedChanged(object sender, EventArgs e)
        {
            m_strwakeuptime = "05:30:00";
            lblAlarm.Text = m_strwakeuptime;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            m_strwakeuptime = "08:00:00";
            lblAlarm.Text = m_strwakeuptime;
        }

        public String GetTimeRemaining(DateTime dtCurrent, String WakeTime)
        {
            // Firstly setup new date time structures for time only, so we can compare the wake time with the current
            // time regardless of date.
            DateTime dtWantedWake;
            DateTime dtWantedWakePlusOneHour = new DateTime();
            DateTime dtWantedWakeToday;

            DateTime dtWakeCurrent = new DateTime();
            dtWakeCurrent = DateTime.Parse(WakeTime);

            DateTime dtTimeCurrent = new DateTime(dtWakeCurrent.Year, dtWakeCurrent.Month, dtWakeCurrent.Day, dtCurrent.Hour, dtCurrent.Minute, dtCurrent.Second);

            // Now we can determine if the wake up time has passed for the current date. If we have passed the wake time for today
            // then the date for the wake time needs to be the next day (tomorrow)
            if (dtTimeCurrent > dtWakeCurrent)
            {
                // Wake time is now tomorrow
                dtWantedWake = new DateTime(dtCurrent.Year, dtCurrent.Month, dtCurrent.Day, dtWakeCurrent.Hour, dtWakeCurrent.Minute, dtWakeCurrent.Second);
                dtWantedWakeToday = new DateTime(dtCurrent.Year, dtCurrent.Month, dtCurrent.Day, dtWakeCurrent.Hour, dtWakeCurrent.Minute, dtWakeCurrent.Second);

                TimeSpan tsFullday = new TimeSpan(24, 0, 0);
                dtWantedWake = dtWantedWake + tsFullday;
            }
            else
            {
                // Wake time is today
                dtWantedWake = new DateTime(dtCurrent.Year, dtCurrent.Month, dtCurrent.Day, dtWakeCurrent.Hour, dtWakeCurrent.Minute, dtWakeCurrent.Second);
                dtWantedWakeToday = new DateTime(dtCurrent.Year, dtCurrent.Month, dtCurrent.Day, dtWakeCurrent.Hour, dtWakeCurrent.Minute, dtWakeCurrent.Second);
            }

            TimeSpan tsTimeRemaining = dtWantedWake - dtCurrent;

            int remmins = tsTimeRemaining.Minutes;
            int remhours = tsTimeRemaining.Hours;
            int remseconds = tsTimeRemaining.Seconds;

            //String strRemaining = (remhours).ToString() + ":" + (remmins).ToString() + ":" + (remseconds).ToString();
            String strRemaining;

            TimeSpan tsSingleHour = new TimeSpan(1, 0, 0);
            dtWantedWakePlusOneHour = dtWantedWakeToday + tsSingleHour;

            if (dtCurrent > dtWantedWakeToday && dtCurrent < dtWantedWakePlusOneHour)
            {
                strRemaining = "None - Rise and shine";
            }
            else
            {
                if (tsTimeRemaining.Hours == 0)
                {
                    if (tsTimeRemaining.Minutes == 0)
                    {
                        strRemaining = tsTimeRemaining.Seconds.ToString() + " seconds";

                        if (strRemaining == "1 seconds")
                        {
                            strRemaining = "1 second";
                        }
                    }
                    else
                    {
                        strRemaining = tsTimeRemaining.Minutes.ToString() + " minutes";

                        if (strRemaining == "1 minutes")
                        {
                            strRemaining = "1 minute";
                        }
                    }
                }
                else
                {
                    strRemaining = tsTimeRemaining.Hours.ToString() + " hours " + tsTimeRemaining.Minutes.ToString() + " minutes";
                }
            }
                        
            return strRemaining;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.ShowDialog();

            txtAction.Text = openFileDialog1.FileName;
        }

        private void btnLaunch_Click(object sender, EventArgs e)
        {
            if (txtAction.Text != "")
            {
                try
                {
                    Process myProcess = new Process();

                    myProcess.StartInfo.FileName = (char)34 + "C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe" + (char)34;
                    myProcess.StartInfo.Arguments = (char)34 + txtAction.Text + (char)34;

                    myProcess.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to start process: " + ex.Message);
                }
            }
        }

        private void chkOntop_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOntop.Checked == true)
            {
                this.TopMost = true ;
            }
            else
            {
                this.TopMost = false;
            }
        }

        private void optCustom_CheckedChanged(object sender, EventArgs e)
        {
            

            try
            {
                DateTime.Parse(txtCustomTime.Text);
                m_strwakeuptime = txtCustomTime.Text;
                lblAlarm.Text = m_strwakeuptime;
            }
            catch (Exception ex)
            {
                m_strwakeuptime = "09:00:00";
                txtCustomTime.Text = "09:00:00";
                lblAlarm.Text = m_strwakeuptime;
            }
        }

        private void txtCustomTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime.Parse(txtCustomTime.Text);
                m_strwakeuptime = txtCustomTime.Text;
                lblAlarm.Text = m_strwakeuptime;
            }
            catch (Exception ex)
            {
                m_strwakeuptime = "09:00:00";
                txtCustomTime.Text = "09:00:00";
                lblAlarm.Text = m_strwakeuptime;
            }
        }

        private void chkAlarmActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAlarmActive.Checked == true)
            {
                lblSleepRemaining.Visible = true;
                lblAlarm.Visible = true;

                opt530.Enabled = true;
                opt545.Enabled = true;
                opt600.Enabled = true;
                opt630.Enabled = true;
                opt645.Enabled = true;
                opt700.Enabled = true;
                opt730.Enabled = true;
                opt800.Enabled = true;
                optCustom.Enabled = true;
                txtAction.Enabled = true;
                btnLaunch.Enabled = true;
                btnBrowse.Enabled = true;
                txtCustomTime.Enabled = true;
                pictureBox2.Visible = true;

            }
            else
            {
                lblSleepRemaining.Visible = false;
                lblAlarm.Visible = false;

                opt530.Enabled = false;
                opt545.Enabled = false;
                opt600.Enabled = false;
                opt630.Enabled = false;
                opt645.Enabled = false;
                opt700.Enabled = false;
                opt730.Enabled = false;
                opt800.Enabled = false;
                optCustom.Enabled = false;
                txtAction.Enabled = false;
                btnLaunch.Enabled = false;
                btnBrowse.Enabled = false;
                txtCustomTime.Enabled = false;
                pictureBox2.Visible = false;
            }
        }
    }
}