namespace AlarmClock
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSleepRemaining = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.tmrTime = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.opt530 = new System.Windows.Forms.RadioButton();
            this.opt545 = new System.Windows.Forms.RadioButton();
            this.opt600 = new System.Windows.Forms.RadioButton();
            this.opt645 = new System.Windows.Forms.RadioButton();
            this.opt630 = new System.Windows.Forms.RadioButton();
            this.opt700 = new System.Windows.Forms.RadioButton();
            this.opt730 = new System.Windows.Forms.RadioButton();
            this.opt800 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtAction = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnLaunch = new System.Windows.Forms.Button();
            this.chkOntop = new System.Windows.Forms.CheckBox();
            this.optCustom = new System.Windows.Forms.RadioButton();
            this.txtCustomTime = new System.Windows.Forms.TextBox();
            this.lblAlarm = new System.Windows.Forms.Label();
            this.chkAlarmActive = new System.Windows.Forms.CheckBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.lblAlarm);
            this.panel1.Controls.Add(this.lblSleepRemaining);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(678, 170);
            this.panel1.TabIndex = 0;
            // 
            // lblSleepRemaining
            // 
            this.lblSleepRemaining.AutoSize = true;
            this.lblSleepRemaining.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSleepRemaining.ForeColor = System.Drawing.Color.Silver;
            this.lblSleepRemaining.Location = new System.Drawing.Point(20, 139);
            this.lblSleepRemaining.Name = "lblSleepRemaining";
            this.lblSleepRemaining.Size = new System.Drawing.Size(170, 24);
            this.lblSleepRemaining.TabIndex = 2;
            this.lblSleepRemaining.Text = "Remaining Time";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.Silver;
            this.lblDate.Location = new System.Drawing.Point(16, 18);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(149, 39);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "CurDate";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.Font = new System.Drawing.Font("Century Gothic", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.Color.Silver;
            this.lbltime.Location = new System.Drawing.Point(10, 57);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(285, 78);
            this.lbltime.TabIndex = 0;
            this.lbltime.Text = "CurTime";
            // 
            // tmrTime
            // 
            this.tmrTime.Enabled = true;
            this.tmrTime.Interval = 400;
            this.tmrTime.Tick += new System.EventHandler(this.tmrTime_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkAlarmActive);
            this.groupBox1.Controls.Add(this.txtCustomTime);
            this.groupBox1.Controls.Add(this.optCustom);
            this.groupBox1.Controls.Add(this.chkOntop);
            this.groupBox1.Controls.Add(this.opt530);
            this.groupBox1.Controls.Add(this.opt545);
            this.groupBox1.Controls.Add(this.opt600);
            this.groupBox1.Controls.Add(this.opt645);
            this.groupBox1.Controls.Add(this.opt630);
            this.groupBox1.Controls.Add(this.opt700);
            this.groupBox1.Controls.Add(this.opt730);
            this.groupBox1.Controls.Add(this.opt800);
            this.groupBox1.Location = new System.Drawing.Point(28, 247);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(613, 83);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Wake up time";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // opt530
            // 
            this.opt530.AutoSize = true;
            this.opt530.Location = new System.Drawing.Point(529, 19);
            this.opt530.Name = "opt530";
            this.opt530.Size = new System.Drawing.Size(67, 17);
            this.opt530.TabIndex = 7;
            this.opt530.TabStop = true;
            this.opt530.Text = "05:30:00";
            this.opt530.UseVisualStyleBackColor = true;
            this.opt530.CheckedChanged += new System.EventHandler(this.opt530_CheckedChanged);
            // 
            // opt545
            // 
            this.opt545.AutoSize = true;
            this.opt545.Location = new System.Drawing.Point(456, 19);
            this.opt545.Name = "opt545";
            this.opt545.Size = new System.Drawing.Size(67, 17);
            this.opt545.TabIndex = 6;
            this.opt545.TabStop = true;
            this.opt545.Text = "05:45:00";
            this.opt545.UseVisualStyleBackColor = true;
            this.opt545.CheckedChanged += new System.EventHandler(this.opt545_CheckedChanged);
            // 
            // opt600
            // 
            this.opt600.AutoSize = true;
            this.opt600.Location = new System.Drawing.Point(383, 19);
            this.opt600.Name = "opt600";
            this.opt600.Size = new System.Drawing.Size(67, 17);
            this.opt600.TabIndex = 5;
            this.opt600.TabStop = true;
            this.opt600.Text = "06:00:00";
            this.opt600.UseVisualStyleBackColor = true;
            this.opt600.CheckedChanged += new System.EventHandler(this.opt600_CheckedChanged);
            // 
            // opt645
            // 
            this.opt645.AutoSize = true;
            this.opt645.Location = new System.Drawing.Point(237, 19);
            this.opt645.Name = "opt645";
            this.opt645.Size = new System.Drawing.Size(67, 17);
            this.opt645.TabIndex = 4;
            this.opt645.TabStop = true;
            this.opt645.Text = "06:45:00";
            this.opt645.UseVisualStyleBackColor = true;
            this.opt645.CheckedChanged += new System.EventHandler(this.opt645_CheckedChanged);
            // 
            // opt630
            // 
            this.opt630.AutoSize = true;
            this.opt630.Location = new System.Drawing.Point(310, 19);
            this.opt630.Name = "opt630";
            this.opt630.Size = new System.Drawing.Size(67, 17);
            this.opt630.TabIndex = 3;
            this.opt630.TabStop = true;
            this.opt630.Text = "06:30:00";
            this.opt630.UseVisualStyleBackColor = true;
            this.opt630.CheckedChanged += new System.EventHandler(this.opt630_CheckedChanged);
            // 
            // opt700
            // 
            this.opt700.AutoSize = true;
            this.opt700.Location = new System.Drawing.Point(164, 19);
            this.opt700.Name = "opt700";
            this.opt700.Size = new System.Drawing.Size(67, 17);
            this.opt700.TabIndex = 2;
            this.opt700.TabStop = true;
            this.opt700.Text = "07:00:00";
            this.opt700.UseVisualStyleBackColor = true;
            this.opt700.CheckedChanged += new System.EventHandler(this.opt700_CheckedChanged);
            // 
            // opt730
            // 
            this.opt730.AutoSize = true;
            this.opt730.Location = new System.Drawing.Point(91, 19);
            this.opt730.Name = "opt730";
            this.opt730.Size = new System.Drawing.Size(67, 17);
            this.opt730.TabIndex = 1;
            this.opt730.TabStop = true;
            this.opt730.Text = "07:30:00";
            this.opt730.UseVisualStyleBackColor = true;
            this.opt730.CheckedChanged += new System.EventHandler(this.opt730_CheckedChanged);
            // 
            // opt800
            // 
            this.opt800.AutoSize = true;
            this.opt800.Location = new System.Drawing.Point(16, 19);
            this.opt800.Name = "opt800";
            this.opt800.Size = new System.Drawing.Size(67, 17);
            this.opt800.TabIndex = 0;
            this.opt800.TabStop = true;
            this.opt800.Text = "08:00:00";
            this.opt800.UseVisualStyleBackColor = true;
            this.opt800.CheckedChanged += new System.EventHandler(this.opt800_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnLaunch);
            this.groupBox2.Controls.Add(this.btnBrowse);
            this.groupBox2.Controls.Add(this.txtAction);
            this.groupBox2.Location = new System.Drawing.Point(28, 185);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(613, 49);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Alarm Action";
            // 
            // txtAction
            // 
            this.txtAction.Location = new System.Drawing.Point(16, 19);
            this.txtAction.Name = "txtAction";
            this.txtAction.Size = new System.Drawing.Size(434, 20);
            this.txtAction.TabIndex = 0;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(456, 18);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(67, 21);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnLaunch
            // 
            this.btnLaunch.Location = new System.Drawing.Point(529, 18);
            this.btnLaunch.Name = "btnLaunch";
            this.btnLaunch.Size = new System.Drawing.Size(67, 21);
            this.btnLaunch.TabIndex = 2;
            this.btnLaunch.Text = "Launch";
            this.btnLaunch.UseVisualStyleBackColor = true;
            this.btnLaunch.Click += new System.EventHandler(this.btnLaunch_Click);
            // 
            // chkOntop
            // 
            this.chkOntop.AutoSize = true;
            this.chkOntop.Location = new System.Drawing.Point(16, 51);
            this.chkOntop.Name = "chkOntop";
            this.chkOntop.Size = new System.Drawing.Size(92, 17);
            this.chkOntop.TabIndex = 3;
            this.chkOntop.Text = "Always on top";
            this.chkOntop.UseVisualStyleBackColor = true;
            this.chkOntop.CheckedChanged += new System.EventHandler(this.chkOntop_CheckedChanged);
            // 
            // optCustom
            // 
            this.optCustom.AutoSize = true;
            this.optCustom.Location = new System.Drawing.Point(164, 50);
            this.optCustom.Name = "optCustom";
            this.optCustom.Size = new System.Drawing.Size(60, 17);
            this.optCustom.TabIndex = 8;
            this.optCustom.TabStop = true;
            this.optCustom.Text = "Custom";
            this.optCustom.UseVisualStyleBackColor = true;
            this.optCustom.CheckedChanged += new System.EventHandler(this.optCustom_CheckedChanged);
            // 
            // txtCustomTime
            // 
            this.txtCustomTime.Location = new System.Drawing.Point(237, 48);
            this.txtCustomTime.Name = "txtCustomTime";
            this.txtCustomTime.Size = new System.Drawing.Size(83, 20);
            this.txtCustomTime.TabIndex = 9;
            this.txtCustomTime.Text = "09:00:00";
            this.txtCustomTime.TextChanged += new System.EventHandler(this.txtCustomTime_TextChanged);
            // 
            // lblAlarm
            // 
            this.lblAlarm.AutoSize = true;
            this.lblAlarm.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlarm.ForeColor = System.Drawing.Color.Silver;
            this.lblAlarm.Location = new System.Drawing.Point(401, 102);
            this.lblAlarm.Name = "lblAlarm";
            this.lblAlarm.Size = new System.Drawing.Size(123, 24);
            this.lblAlarm.TabIndex = 3;
            this.lblAlarm.Text = "Alarm Time";
            // 
            // chkAlarmActive
            // 
            this.chkAlarmActive.AutoSize = true;
            this.chkAlarmActive.Checked = true;
            this.chkAlarmActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAlarmActive.Location = new System.Drawing.Point(343, 50);
            this.chkAlarmActive.Name = "chkAlarmActive";
            this.chkAlarmActive.Size = new System.Drawing.Size(84, 17);
            this.chkAlarmActive.TabIndex = 10;
            this.chkAlarmActive.Text = "Alarm active";
            this.chkAlarmActive.UseVisualStyleBackColor = true;
            this.chkAlarmActive.CheckedChanged += new System.EventHandler(this.chkAlarmActive_CheckedChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::AlarmClock.Properties.Resources.Alarmicon1;
            this.pictureBox2.Location = new System.Drawing.Point(387, 102);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(18, 17);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AlarmClock.Properties.Resources.Moonpic;
            this.pictureBox1.Location = new System.Drawing.Point(530, 52);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(148, 119);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(677, 342);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Alarm Clock v1.00";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer tmrTime;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton opt530;
        private System.Windows.Forms.RadioButton opt545;
        private System.Windows.Forms.RadioButton opt600;
        private System.Windows.Forms.RadioButton opt645;
        private System.Windows.Forms.RadioButton opt630;
        private System.Windows.Forms.RadioButton opt700;
        private System.Windows.Forms.RadioButton opt730;
        private System.Windows.Forms.RadioButton opt800;
        private System.Windows.Forms.Label lblSleepRemaining;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtAction;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnLaunch;
        private System.Windows.Forms.CheckBox chkOntop;
        private System.Windows.Forms.TextBox txtCustomTime;
        private System.Windows.Forms.RadioButton optCustom;
        private System.Windows.Forms.Label lblAlarm;
        private System.Windows.Forms.CheckBox chkAlarmActive;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

